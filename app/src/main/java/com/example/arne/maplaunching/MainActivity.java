package com.example.arne.maplaunching;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void showMap(View view)
    {
        //get data values
        EditText editText1= (EditText) findViewById(R.id.lat);
        EditText editText2= (EditText) findViewById(R.id.lng);
        String n1 = editText1.getText().toString();
        String n2 = editText2.getText().toString();
        double lat = Double.parseDouble(n1);
        double lng = Double.parseDouble(n2);
        //open map
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("geo:"+lat+","+lng));
        startActivity(intent);
    }
}

